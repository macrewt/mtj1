<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTradesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trades', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->string('trade_type')->nullable();
            $table->date('trade_date')->nullable();
            $table->string('am_trading')->nullable();
            $table->text('specify_trade')->nullable();
            $table->text('feeling_today')->nullable();
            $table->string('headspace_today')->nullable();
            $table->string('price_action')->nullable();
            $table->string('support_resistance')->nullable();
            $table->text('specify_support_resistance')->nullable();
            $table->string('trendline')->nullable();
            $table->text('specify_trendline')->nullable();
            $table->text('indicators')->nullable();
            $table->double('entry_price', 8, 2)->default(0);
            $table->string('sl')->nullable();
            $table->string('tp')->nullable();
            $table->string('status')->nullable();
            $table->text('trade_playout')->nullable();
            $table->string('trade_playout_status')->nullable();
            $table->double('trade_playout_amout', 8, 2)->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trades');
    }
}
