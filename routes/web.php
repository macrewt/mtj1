<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
    return view('auth.login');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');

    Route::get('/users/list', 'UsersController@list')->name('users.list');
    Route::get('/permissions/list', 'PermissionsController@list')->name('permissions.list');

    Route::resource('permissions', 'PermissionsController');
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');

    Route::resource('roles', 'RolesController');
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');

    Route::resource('users', 'UsersController');
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');

});

Auth::routes();

//Auth::routes(['verify' => true]);

Route::get('email/verify/{id}/{hash}', 'Api\v1\VerificationController@verify')->name('verification.verify');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/pinreset', 'Auth\ResetpinController@showform')->name('pin.reset');
Route::post('/savepin', 'Auth\ResetpinController@reset')->name('pin.request');
