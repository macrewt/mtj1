<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

Route::group([
    'middleware' => 'api',
    'prefix' => 'v1',
    'namespace' => 'Api\v1'

], function ($router) {
	Route::post('register', 'RegisterController@register');
	Route::post('login', 'LoginController@login');
    Route::post('forgotpassword', 'LoginController@forgotpassword');
    Route::post('/login/{provider}/callback', 'SocialLoginController@handleProviderCallback');

});

Route::group(['middleware' => 'auth.jwt', 'prefix' => 'v1', 'namespace' => 'Api\v1' ], function ($router) {

    Route::get('email/resend', 'VerificationController@resend');

    Route::middleware(['emailverified'])->group(function () {
        Route::post('logout', 'LoginController@logout');
        Route::get('refresh', 'RegisterController@refresh');
        Route::post('editprofile', 'RegisterController@editprofile');
        Route::post('setpin', 'RegisterController@setpin');
        Route::post('forgotpin', 'LoginController@forgotpin');
        Route::apiResource('trades', 'TradesController');
        Route::apiResource('userdetails', 'UserDetailsController');
    });
});


