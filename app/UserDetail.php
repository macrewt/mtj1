<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
    protected $fillable = [  
        'user_id',
        'notes',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
