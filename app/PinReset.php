<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\ForgotPin;

class PinReset extends Model
{

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $fillable = [
        'email',
        'token',
        'created_at',
    ];
}
