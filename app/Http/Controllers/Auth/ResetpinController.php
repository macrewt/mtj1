<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;

use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\Password;

use Auth;
use App\User;
use App\RoleUser;
use App\PinReset;
use Validator;

class ResetpinController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */


    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */

    public function __construct()
    {
        $this->middleware('guest');
    }

    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }

    public function showform(Request $request){
        $token = $request->token;
        $email = $request->email;

        return view('auth.pins.reset', compact('email', 'token'));
        
    }

    public function reset(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'token' => 'required', 
            'email' => 'required|email',
            'pin' => 'required|min:4|digits:4|integer|confirmed',
        ]);

        if ($validator->fails()) {

            $messages = $validator->messages();
            //return Redirect::to('/pinreset')->with('message', 'Register Failed');
            return redirect()->back()
                    ->withErrors($validator->errors());
        }

        $query = PinReset::where('email', $request->email)->where('token', $request->token);

        if($query->count() > 0){
            $pinreset = $query->first();
            $expiretime =  date('Y-m-d H:i:s', strtotime('+1 hour',strtotime($pinreset->created_at)));

            if(date('Y-m-d H:i:s') < $expiretime){
                $user = User::where('email', $request->email)->first();
                $params = $request->all();
                $user->fill($params);
                $update = $user->save();

                $pinreset->delete();
                return redirect('/')->with('success', 'pin is set successfully.');
            } else {
                return redirect('/')->with('error', 'Link is expired. Please try again.');
            }

        } else {
            return redirect('/')->with('error', 'Link is expired. Please try again.');
        }

        
        

        
    }

    
}
