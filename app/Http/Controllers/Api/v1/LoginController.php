<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\ApiController;
use Illuminate\Http\Request;

use App\User;
use App\RoleUser;
use App\PinReset;

use Validator;
use Response;
use App\Http\Requests;

use JWTFactory;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use App\Http\Requests\ResetPasswordRequest;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

use Notification;
use App\Notifications\ForgotPin;

class LoginController extends ApiController
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'forgotpassword']]);
    }

    public function login(Request $request){

        try{
	        $validator = Validator::make($request->all(), [
	            'email' => 'required|string|email|max:255',
	            //'phone' => 'required',
	            'password'=> 'required'
	        ]);

	        if ($validator->fails()) {
	            return $this->payload([
	                'StatusCode' => '422',
	                'message' => $validator->errors(),
	                'result' => new \stdClass
	            ], 200);
	        }
        $credentials = $request->only('email', 'password');
        //$credentials = $request->only('phone', 'password');
        //$device_token = $request->device_token;
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return $this->payload([
                    'StatusCode' => '401',
                    'message' => 'Invalid credentials',
                    'result' => new \stdClass
                ], 200);
            }
        } catch (JWTException $e) {
            return $this->payload([
                'StatusCode' => '500',
                'message' => 'Could not create token',
                'result' => new \stdClass
             ], 200);
        }
        $user = JWTAuth::user();
        if (!$user->hasVerifiedEmail()) {
            $user->sendEmailVerificationNotification();
            return response()->json([
                'StatusCode' => '500',
                'message' => 'Email verification link sent on your email id',
                'result' => new \stdClass
            ],200);
        }

        $user = $user->load(['roles', 'user_detail']);
        $user_id = $user->id;
        $user->token = $token;
        //$user->roles = $user->roles;
        $total_login = $user->total_login + 1;
        $user->total_login = $total_login;
        $user->last_login = date('Y-m-d H:i:s');
        User::update_user(
        	array('last_login'=>date('Y-m-d H:i:s'), 'total_login'=>$total_login), array('id'=>$user_id)
        );

        return $this->payload([
            'StatusCode' => '200',
            'message' => 'Login successful!',
            'result' => array('user' => $user)
        ], 200);

        } catch(Exception $e) {
            return $this->payload(['StatusCode' => '422', 'message' => $e->getMessage(), 'result' => new \stdClass], 200);
        }
    }

    public function logout(Request $request){
        $user = JWTAuth::user();
        $id = $user->id;
        JWTAuth::invalidate();
        return $this->payload([
            'StatusCode' => '200',
            'message' => 'Logged out Successfully.',
            'result' => new \stdClass
        ], 200);

    }

    public function forgotpassword(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email|max:255',
        ]);

        if ($validator->fails()) {
            return $this->payload([
                'StatusCode' => '422',
                'message' => $validator->errors(),
                'result' => new \stdClass
            ], 200);
        }


        try {
            $response = Password::sendResetLink($request->only('email'), function (Message $message) {
                $message->subject($this->getEmailSubject());
            });
            switch ($response) {
                case Password::RESET_LINK_SENT:
                    return $this->payload([
                        'StatusCode' => '200',
                        'message' => trans($response),
                        'result' => new \stdClass
                    ], 200);
                    //return \Response::json(array("status" => 200, "message" => trans($response), "data" => array()));
                case Password::INVALID_USER:
                    return $this->payload([
                        'StatusCode' => '400',
                        'message' => trans($response),
                        'result' => new \stdClass
                    ], 200);
                    //return \Response::json(array("status" => 400, "message" => trans($response), "data" => array()));
            }
        } catch (\Swift_TransportException $e) {

            return $this->payload([
                'StatusCode' => '400',
                'message' => $e->getMessage(),
                'result' => new \stdClass
            ], 200);

            //$arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
        } catch (Exception $e) {
            return $this->payload([
                'StatusCode' => '400',
                'message' => $e->getMessage(),
                'result' => new \stdClass
            ], 200);
            //$arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
        }

    }

    public function forgotpin(Request $request){

        try{

            $user = JWTAuth::user();
            $token = Str::random(32);
            $pinreset = PinReset::create(['email' => $user->email, 'token' => $token, 'created_at' => date('Y-m-d H:i:s')]);

            $details = [
                'email' => $pinreset->email,
                'token' => $pinreset->token
            ];

            $response = $user->notify(new ForgotPin($details));
            return $this->payload([
                'StatusCode' => '200',
                'message' => "We have emailed your pin reset link!",
                'result' => new \stdClass
            ], 200);




        } catch(Exception $e) {
            return $this->payload(['StatusCode' => '422', 'message' => $e->getMessage(), 'result' => new \stdClass], 200);
        }

    }
}
