<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\ApiController;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTFactory;
use JWTAuth;
use App\Trade;
use Validator;

class TradesController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function index(Request $request)
    {
        $user = JWTAuth::user();
        $keyword = !empty($request->keyword) ? $request->keyword : '';
        $query = Trade::with(['trade_images'])->where('user_id', $user->id);

        if(!empty($keyword)){
            $query =  $query->where(function ($query) use ($keyword) {
                $query->where('specify_trade', 'LIKE', '%'.$keyword.'%');
                $query->orWhere('feeling_today', 'LIKE', '%'.$keyword.'%');
                $query->orWhere('specify_support_resistance', 'LIKE', '%'.$keyword.'%');
                $query->orWhere('specify_trendline', 'LIKE', '%'.$keyword.'%');
                $query->orWhere('trade_playout', 'LIKE', '%'.$keyword.'%');
            });
        }

        $trades = $query->orderBy('id', 'DESC')->get();

        return $this->payload([
            'StatusCode' => '200',
            'message' => 'Trades Listing',
            'result' => array('Trades' => $trades)
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = JWTAuth::user();

        $validator = Validator::make($request->all(), [
            'am_trading'=> 'required',
            'specify_trade' => 'required',
            'entry_price' => 'required',
            'sl' => 'required',
            'tp' => 'required',
            'images.*' => 'mimes:jpeg,jpg,png,gif'
        ]);

        $validator->sometimes(['trade_playout_status', 'trade_playout_amout'], 'required', function ($input) {
            return $input->status === 'finish';
        });


        if ($validator->fails()) {
            return $this->payload(['StatusCode' => '422', 'message' => $validator->errors(), 'result' => new \stdClass], 200);
        }

        $input = $request->all();
        $input['user_id'] = $user->id;

        $trade = Trade::create($input);

        if($request->has('images') && count($request->images) > 0){
            foreach($request->images as $image){
                $trade->trade_images()->create(['image' => $image]);
            }
        }
        $trade->load('trade_images');

        return $this->payload([
            'StatusCode' => '200',
            'message' => 'Trade saved successfully',
            'result' => array('trade' => $trade)
        ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $trade = Trade::with(['trade_images'])->find($id);

        return $this->payload([
            'StatusCode' => '200',
            'message' => 'Trade details',
            'result' => array('trade' => $trade)
        ],200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = JWTAuth::user();
        $trade = Trade::find($id);

        $validator = Validator::make($request->all(), [
            'am_trading'=> 'required',
            'specify_trade' => 'required',
            'entry_price' => 'required',
            'sl' => 'required',
            'tp' => 'required',
            'images.*' => 'mimes:jpeg,jpg,png,gif'
        ]);
        $validator->sometimes(['trade_playout_status', 'trade_playout_amout'], 'required', function ($input) {
            return $input->status === 'finish';
        });

        if ($validator->fails()) {
            return $this->payload(['StatusCode' => '422', 'message' => $validator->errors(), 'result' => new \stdClass], 200);
        }

        $input = $request->all();
        $trade->update($input);
        
        if($request->has('images') && count($request->images) > 0){
            foreach($request->images as $image){
                $trade->trade_images()->create(['image' => $image]);
            }
        }
        $trade->load('trade_images');

        return $this->payload([
            'StatusCode' => '200',
            'message' => 'Trade updated successfully',
            'result' => array('trade' => $trade)
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $trade = Trade::find($id);
        $result =   $trade->delete();
        return $this->payload([
            'StatusCode' => '200',
            'message' => 'Deleted successfully',
            'result' => new \stdClass
        ],200);
    }
}
