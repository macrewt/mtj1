<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\ApiController;
use App\Http\Controllers\Controller;
use App\RoleUser;
use App\SocialIdentity;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use Auth;
use Exception;
use App\User;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\JWT;
use JWTFactory;
use JWTAuth;

class SocialLoginController extends ApiController
{
    public function redirectToProvider($provider)
    {

        return Socialite::driver($provider)->stateless()->redirect();
    }

    public function handleProviderCallback($provider, Request $request)
    {
//        try {
//            $user = Socialite::driver($provider)->user();
//        }catch(Exception $e){
//            return $this->payload(['StatusCode' => '422', 'message' => $e->getMessage(), 'result' => new \stdClass], 200);
//        }
//        $user =
        $authUser = $this->findOrCreateUser($request, $provider);
        $token = JWTAuth::fromUser($authUser);

        $authUser->token = $token;
//        User::login($authUser, true);
           User::update_user(array('last_login' => date('Y-m-d H:i:s'), 'total_login' => $authUser->total_login + 1), array('id' => $authUser->id));
        return $this->payload([
            'StatusCode' => '200',
            'message' => 'Login successful!',
            'result' => array('user' => $authUser)
        ], 200);
    }


    public function findOrCreateUser($providerUser, $provider)
    {
//        $account = SocialIdentity::whereProviderName($provider)
//            ->whereProviderId($providerUser->getId())
//            ->first();
//        $account = SocialIdentity::where('provider_id', $providerUser->social_id)->where('provider_name', $provider)->first();
//        if ($account) {
//            return $account->user;
//        } else {
            $user = User::whereHas('identities', function ($q) use ($provider,$providerUser){
                $q->where('provider_name' , $provider)->where('provider_id',$providerUser->social_id);
            })->first();

            if (!$user) {
//                $email = $provider == 'twitter' || empty($providerUser->email) ? $providerUser->social_id . '@' . $provider . '.com' : $providerUser->email;
                $user = User::create([
                    'email' => $providerUser->email,
                    'name' => $providerUser->name,
                    'password' => Hash::make($providerUser->name)
                ]);
            }
            $user->identities()->updateOrCreate(['user_id'=>$user->id],[
                'provider_id' => $providerUser->social_id,
                'provider_name' => $provider,
            ]);
            return $user;
//        }
    }
}
