<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Api\v1\ApiController;
use Illuminate\Http\Request;

use App\User;
use App\RoleUser;

use Validator;
use Response;
use App\Http\Requests;

use JWTFactory;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

use Illuminate\Foundation\Auth\VerifiesEmails;
use Carbon\Carbon;

class VerificationController extends ApiController
{

	public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'verify']]);
    }

    public function verify($user_id, Request $request) {


	    if (!$request->hasValidSignature()) {
	        return $this->payload([
                'StatusCode' => '401',
                'message' => 'Invalid/Expired url provided.', 
                'result' => new \stdClass
             ], 200);
	    }

	    $user = User::findOrFail($user_id);

	    if (!$user->hasVerifiedEmail()) {
	    	User::update_user(
	        	array('email_verified_at'=>date('Y-m-d H:i:s')), array('id'=>$user->id)
	        );
	        //$user->markEmailAsVerified();
	    }

	    $iPod    = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
		$iPhone  = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
		$iPad    = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
		$iOS    = stripos($_SERVER['HTTP_USER_AGENT'],"iOS");
		$Android = stripos($_SERVER['HTTP_USER_AGENT'],"Android");
		$webOS   = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");

		$message = 'Your email has been successfully verified, you can login with your details in Android and iOS app' ;

		echo  $message;

		if( $iPod || $iPhone || $iPad || $iOS){
		    //browser reported as an iPhone/iPod touch -- do something here
		    header( "refresh:2; url=mtj://" );
		} else if($Android) {
		    //browser reported as a webOS device -- do something here
		    header( "refresh:2; url=mtj://mtj" );
		} else {

		}

	    //return redirect()->away('mtj://mtj');
	}

	public function resend(Request $request) {

		$user = JWTAuth::user();

	    if ($user->hasVerifiedEmail()) {
	        return $this->payload([
	            'StatusCode' => '400',
	            'message' => 'Email already verified.', 
	            'result' => new \stdClass
	        ], 200);
	    }

	    
	    $user->sendEmailVerificationNotification();

	    //auth()->user()->sendEmailVerificationNotification();
	    return $this->payload([
            'StatusCode' => '200',
            'message' => 'Email verification link sent on your email id', 
            'result' => new \stdClass
        ], 200);

	    //return response()->json(["msg" => "Email verification link sent on your email id"]);
	}
}
