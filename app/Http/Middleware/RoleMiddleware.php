<?php

namespace App\Http\Middleware;

use Closure;

use JWTFactory;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::user();
        $user = $user->load('roles');

        foreach( $user->roles as $role ) {
          if( $user->hasRole($role) )
            return $next($request);
        }

        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json([
                        'StatusCode' => '500',
                        'message' => 'Not authorized',
                        'result' => new \stdClass
                    ], 200);
            }

        } catch (Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

                return response()->json([
                    'StatusCode' => '500',
                    'message' => 'Token expired',
                    'result' => new \stdClass
                ], 200);

        } catch (Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

                return response()->json([
                    'StatusCode' => '500',
                    'message' => 'Token invalidate',
                    'result' => new \stdClass
                ], 200);

        } catch (Tymon\JWTAuth\Exceptions\JWTException $e) {

                return response()->json([
                    'StatusCode' => '500',
                    'message' => 'Token absent',
                    'result' => new \stdClass
                ], 200);

        }

        return response()->json([
            'StatusCode' => '500',
            'message' => 'Not authorized',
            'result' => new \stdClass
        ],200);
    }
}
