<?php

namespace App\Http\Middleware;

use Closure;

use JWTFactory;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class EmailVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = JWTAuth::user();
        if (!$user->hasVerifiedEmail()) {
            return response()->json([
                'StatusCode' => '500',
                'message' => 'Email not verified',
                'result' => new \stdClass
            ],200);
        }
        return $next($request);
    }
}
