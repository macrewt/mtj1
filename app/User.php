<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Carbon\Carbon;
use Hash;
use Illuminate\Auth\Notifications\ResetPassword;
use DB;

use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Trade;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'name',
        'email',
        'phone',
        'password',
        'pin',
        'pin_enabled',
        'created_at',
        'updated_at',
        'deleted_at',
        'remember_token',
        'email_verified_at',
        'is_phone_verified',
        'device_token',
        'isd_code',
        'last_login',
        'total_login',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $appends = ['balance'];

    public function getEmailVerifiedAtAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setEmailVerifiedAtAttribute($value)
    {
        $this->attributes['email_verified_at'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function setPasswordAttribute($input)
    {
        if ($input) {
            $this->attributes['password'] = app('hash')->needsRehash($input) ? Hash::make($input) : $input;
        }
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token));
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function role_user(){
        return $this->hasMany('App\RoleUser');
    }

    public function user_detail()
    {
        return $this->hasOne('App\UserDetail')->withDefault([
            'notes' => null,
        ]);
    }

    public static function laratablesCustomRoles($user)
    {
        return view('admin.users.roles', compact('user'))->render();
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function hasRole( $role ) {
        $roles =  array_column($this->roles->toArray(), 'title');
        $roles = array_map('strtolower', $roles);
        return ( in_array($role, $roles) ) ? true : false ;
    }

    public static function laratablesCustomAction($user)
    {
        return view('admin.users.action', compact('user'))->render();
    }

    public static function update_user($input, $where){
        $query = User::where($where);
        if($query->update($input)) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function getBalanceAttribute() {
        $profit = Trade::where('user_id', $this->id)
            ->where('status', 'finish')
            ->where('trade_playout_status', 'profit')->sum('trade_playout_amout');

        $loss = Trade::where('user_id', $this->id)
            ->where('status', 'finish')
            ->where('trade_playout_status', 'loss')->sum('trade_playout_amout');

        $balance = $profit - $loss;
        return $balance;
    }

    public function identities() {
        return $this->hasMany('App\SocialIdentity');
    }


}
