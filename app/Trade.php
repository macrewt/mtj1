<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Trade extends Model implements \Czim\Paperclip\Contracts\AttachableInterface
{
    use \Czim\Paperclip\Model\PaperclipTrait, SoftDeletes;

    protected $fillable = [  
        'user_id',
        'trade_type',
        'trade_date',
        'am_trading',
        'specify_trade',
        'feeling_today',
        'headspace_today',
        'price_action',
        'support_resistance',
        'specify_support_resistance',
        'trendline',
        'specify_trendline',
        'indicators',
        'entry_price',
        'sl',
        'tp',
        'status',
        'trade_playout',
        'trade_playout_status',
        'trade_playout_amout',
        'image',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
        'deleted_at',
    ];

    protected $appends = ['trade_image_url'];

    public function __construct( array $attributes = [] ) {
        $this->hasAttachedFile('image');
        parent::__construct($attributes);
    }

    public function getTradeImageUrlAttribute() {
        return $this->image->url();
    }

    public function user()
    {
        return $this->belongsTo('App\User')->withTrashed();
    }

    public function trade_images(){
        return $this->hasMany('App\TradeImage');
    }
}
