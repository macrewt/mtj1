<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TradeImage extends Model implements \Czim\Paperclip\Contracts\AttachableInterface
{
    use \Czim\Paperclip\Model\PaperclipTrait;

    protected $fillable = [  
        'trade_id',
        'image',
        'created_at',
        'updated_at',
    ];

    protected $dates = [
        'updated_at',
        'created_at',
    ];

    protected $appends = ['image_url'];

    public function __construct( array $attributes = [] ) {
        $this->hasAttachedFile('image');
        parent::__construct($attributes);
    }

    public function getImageUrlAttribute() {
        return $this->image->url();
    }

    public function trade()
    {
        return $this->belongsTo('App\Trade')->withTrashed();
    }
}
